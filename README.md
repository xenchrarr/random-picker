# Random picker

A simple script that will select x random from a file. 

Originally created to be able to select x number of dogs from the kennel.
It was later extended to support to split the random selected into y 
number of groups. Everything is random, if you enter 12 items and 
4 groups, it will pick 12 random, and randomly put them into 4 groups. 

## Functions

- It supports to split that selection into y number of equal groups 

- It can either print it back to you, or write it to a file. 

- The file needs to have one entry per line. 

### Input
A text file where every entry is on a separate line. A single line will 
be one entry in the list. 

### Output 
The output file (if selected) will contain the random selected. Either 
one list with items or multiple list with random elements.

### usage:


python3.6 main.py
```
-i <input_file> 

-o <output_file>

-n <number_of_items_you_want_to_return> 

-s <number_of_equal_splits>
```
**example:**

```
python3.6 main.py -i example.txt -n 10 -s 2 -o output.txt
```

### Rules

- `-i` must be used. 

- if `-s` is used without `-n` it will split the group down to `-s` from all of the items in the supplied file. 

- if `-n` is used without `-s` it will use the number from all of the items in the file

- if `-s` is not used, `-n` have to be used. 