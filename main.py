import sys
import getopt
import random
import os.path


def get_dogs(file, number=0, split=False, split_number=0):
    dogs = read_file(file)

    check_errors(dogs, number, split, split_number)

    if split:
        return get_multiple_groups(dogs, number, split_number)

    return sample(iter(dogs), number)


def get_multiple_groups(dogs, number, split_number):
    number_of_dogs = number
    if number == 0:
        number_of_dogs = len(dogs)
    size = number_of_dogs / split_number
    dogs_left = dogs
    new_dog_list = []
    for x in range(0, split_number):
        new_dogs = sample(iter(dogs_left), int(size))
        new_dog_list.append(new_dogs)
        dog_to_remove = []
        for dog in dogs_left:
            for dog1 in new_dogs:
                if dog == dog1:
                    dog_to_remove.append(dog)
        for dog in dog_to_remove:
            dogs_left.remove(dog)
    return new_dog_list


def check_errors(dogs, number, split, split_number):
    if number > len(dogs):
        print('-n (number you want pick out) is larger than the list')
        sys.exit()

    if number < 0:
        print('Come on... ' + number + '??')
        sys.exit()

    if split:
        if number != 0:
            if split_number > number:
                print("Split is larger than total available")
                sys.exit()

        if split_number <= 1:
            print("Stupid split number")
            sys.exit()

        if split_number > len(dogs):
            print("Split is larger than total available")
            sys.exit()


def sample(iterator, k):
    result = [next(iterator) for _ in range(k)]

    n = k - 1
    for item in iterator:
        n += 1
        s = random.randint(0, n)
        if s < k:
            result[s] = item

    return result


def read_file(file):
    if os.path.exists(file):
        if not os.path.isfile(file):
            print('Input file is a folder')
            sys.exit()

    else:
        print('Input file does not exist')
        sys.exit()

    with open(file) as f:
        content = f.readlines()
        content = [x.strip('\n') for x in content]
    return content


def print_dogs(dogs, split=False):
    print("The selected dogs are: \n")

    if not split:
        for dog in dogs:
            print(dog + "\n")
    else:
        i = 1
        for group in dogs:
            print("Group " + str(i) + "\n")
            for dog in group:
                print(dog)
            print("\n")
            i += 1


def print_help():
    print('test.py -i <input file> -o <output file> -n <number of dogs> -s <number of equal splits>')
    print('Input file should have every separate entry on a new line')


def write_output_file(output_file, dogs, split=False):
    if os.path.exists(output_file):
        if os.path.isfile(output_file):
            choice = input(output_file + " already exists, do you want to overwrite it? y/n - ")

            if choice == 'n':
                new_file = input("Enter new file to write to - ")
                write_output_file(new_file, dogs, split)
                return

            elif choice != 'y':
                print("wrong input, exiting")
                sys.exit()
        else:
            new_file = input("Output file is a folder, enter new output file - ")
            write_output_file(new_file, dogs, split)
            return

    f = open(output_file, 'w')

    if not split:
        for dog in dogs:
            f.write(dog + '\n')
        f.close()

    else:
        i = 1
        for group in dogs:
            f.write("Group: " + str(i) + "\n\n")
            for dog in group:
                f.write(dog + '\n')
            f.write("\n\n")
            i += 1
        f.close()


def main(argv):
    input_file = ''
    output_file = ''
    number = 0
    split = False
    split_number = 0
    try:
        opts, args = getopt.getopt(argv, "h:i:o:n:s:", ["ifile=", "ofile=", "number=", "split="])
    except getopt.GetoptError:
        print_help()
        sys.exit(2)

    if len(argv) == 0:
        print_help()
        sys.exit()
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()

        elif opt in ("-i", "--ifile"):
            input_file = arg
      
        elif opt in ("-o", "--ofile"):
            output_file = arg

        elif opt in ("-n", "--number"):
            try:
                int(arg)
            except ValueError:
                print(arg + ' is not a number')
                sys.exit()
            number = int(arg)

        elif opt in ("-s", "--split"):
            split = True
            try:
                int(arg)
            except ValueError:
                print(arg + " is not a number")
                sys.exit()
            split_number = int(arg)

    take_action(input_file, number, output_file, split, split_number)


def take_action(input_file, number, output_file, split, split_number):
    if input_file == '' or (not split and number == 0) or (split and (split_number > number != 0)):
        print_help()
        sys.exit()

    dogs = get_dogs(input_file, number, split, split_number)
    if output_file != '':
        write_output_file(output_file, dogs, split)
    else:
        print_dogs(dogs, split)


if __name__ == "__main__":
    main(sys.argv[1:])
